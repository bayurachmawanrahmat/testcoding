//
//  AppDelegate.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    //MARK: set First ViewController
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      // Override point for customization after application launch.
      let mainStoryboard = UIStoryboard(name: StoryboardName.Main.rawValue, bundle: nil)
      let allVC = mainStoryboard.instantiateViewController(withIdentifier: StoryboardId.DashboardVC.rawValue)
      let rootViewController = UINavigationController(rootViewController: allVC)
      window = UIWindow(frame: UIScreen.main.bounds)
      window?.rootViewController = rootViewController
      window?.makeKeyAndVisible()
      return true
    }
    
    
    /*
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    */

}

