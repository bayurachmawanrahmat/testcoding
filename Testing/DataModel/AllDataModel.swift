//
//  AllDataModel.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation

extension DataModels: DataCell{
  
}

final class AllDataViewModel {
  private var listAll: [DataModels] = []
  private let apiService = APIService()
  
  var numRowsAllData: Int {return listAll.count}
  
  public func getAllData(from indexPath: IndexPath) -> DataModels{
    return listAll[indexPath.row]
  }
  
  public func reqGetAllData(isLoad: @escaping(Bool) -> Void, completion: @escaping() -> Void){
    isLoad(true)
    listAll = []
    apiService.getRequestAllData { [self] (results) in
      switch results {
      case .Success(let dataResult): self.mapResponseToDomain(from: dataResult)
      case .Failure(let error): fatalError(error.localizedDescription)
      }
      isLoad(false)
      completion()
    }
  }
  
  private func mapResponseToDomain(from data: Data){
    do {
      let allResponse = try JSONDecoder().decode(AllResponse.self, from: data)
        for produk in allResponse.data {
            listAll.append(
                DataModels(
                    productName: produk.productName,
                    productLogo: produk.productLogo,
                    description: produk.description,
                    rating: produk.rating,
                    latestVersion: produk.latestVersion,
                    publisher: produk.publisher,
                    colorTheme: produk.colorTheme
              )
            )
        }
    } catch let error {
      print(error)
    }
  }
}
