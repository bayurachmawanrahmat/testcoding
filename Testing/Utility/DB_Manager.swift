//
//  DB_Manager.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation
import SQLite3

class DB_Manager
{
    init()
    {
        db = openDatabase()
        createTable()
    }

    let dbPath: String = "myDb.sqlite"
    var db:OpaquePointer?

    func openDatabase() -> OpaquePointer?
    {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(dbPath)
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK
        {
            print("error opening database")
            return nil
        }
        else
        {
            print("Successfully opened connection to database at \(dbPath)")
            return db
        }
    }
    
    func createTable() {
        let createTableString = "CREATE TABLE IF NOT EXISTS favorite(id INTEGER PRIMARY KEY,name TEXT,desc TEXT,pub TEXT,vers TEXT,rat TEXT,logo TEXT,bgcolor TEXT);"
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK
        {
            if sqlite3_step(createTableStatement) == SQLITE_DONE
            {
                print("favorite table created.")
            } else {
                print("favorite table could not be created.")
            }
        } else {
            print("CREATE TABLE favorite could not be prepared.")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    
    func insert(id:Int, name:String, desc:String,pub:String,vers:String,rat:String,logo:String,bgcolor:String)
    {
        let persons = read()
        for p in persons
        {
            if p.id == id
            {
                return
            }
        }
        let insertStatementString = "INSERT INTO favorite (id,name,desc,pub,vers,rat,logo,bgcolor) VALUES (NULL,?,?,?,?,?,?,?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (name as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 2, (desc as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 3, (pub as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 4, (vers as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 5, (rat as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 6, (logo as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 7, (bgcolor as NSString).utf8String, -1, nil)
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    func read() -> [FavoriteModel] {
        let queryStatementString = "SELECT * FROM favorite;"
        var queryStatement: OpaquePointer? = nil
        var psns : [FavoriteModel] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let id = sqlite3_column_int(queryStatement, 0)
                let name = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let desc = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let pub = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let vers = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let rat = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let logo = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let bgcolor = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                psns.append(FavoriteModel(id: Int(id), name: name, desc: desc, pub: pub, vers: vers, rat: rat, logo: logo, bgcolor: bgcolor))
                print("Query Result:")
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return psns
    }
    
    func deleteByID(id:Int) {
        let deleteStatementStirng = "DELETE FROM favorite WHERE id = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(id))
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
    }
    
}
