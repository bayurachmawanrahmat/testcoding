//
//  Constant.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation

final class Constant {
  public static let BASE_URL = "https://apimocha.com/telkom/v2/products"
}

enum ResponseResult<T,F> {
  case Success(T)
  case Failure(F)
}

enum StoryboardName: String {
  case Main
}

enum StoryboardId: String {
  case DashboardVC
}
