//
//  APIService.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation
import Alamofire

final class APIService {
  public func getRequestAllData(completion: @escaping(ResponseResult<Data, Error>) -> Void){
    let routerAllData = APIRouter.getAllData
    AF.request(routerAllData)
      .validate()
      .responseData { (response) in
        switch response.result {
        case .success(let dataResponse): completion(.Success(dataResponse))
        case .failure(let error): completion(.Failure(error))
        }
        
      }
                                    
  }
}
