//
//  APIRouter.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
  case getAllData
  
  var method: HTTPMethod{
    switch self{
    case .getAllData: return .post
    }
  }
  
  func asURLRequest() throws -> URLRequest {
    var urlRequest: URLRequest?
    var parameter: Parameters = Parameters()
    
    if let URL_BASE = URL(string: Constant.BASE_URL){
      switch self{
      case .getAllData:
        urlRequest = URLRequest(url: URL_BASE)
        urlRequest?.httpMethod = method.rawValue
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Content-Type")
      }
    }
    
    return try URLEncoding.default.encode(urlRequest!, with: parameter)
  }
}

