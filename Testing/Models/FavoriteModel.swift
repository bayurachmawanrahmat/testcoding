//
//  FavoriteModel.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation
class FavoriteModel
{
    var id: Int = 0
    var name: String = ""
    var desc: String = ""
    var pub: String = ""
    var vers: String = ""
    var rat: String = ""
    var logo: String = ""
    var bgcolor: String = ""
    
    
    init(id:Int, name:String, desc:String, pub:String, vers:String, rat:String, logo:String, bgcolor:String)
    {
        self.id = id
        self.name = name
        self.desc = desc
        self.pub = pub
        self.vers = vers
        self.rat = rat
        self.logo = logo
        self.bgcolor = bgcolor
        
    }
    
}
