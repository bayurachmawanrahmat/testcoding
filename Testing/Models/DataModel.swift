//
//  DataModel.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation

struct DataModels {
    let productName: String
    let productLogo: String
    let description: String
    let rating: Double
    let latestVersion: String
    let publisher: String
    let colorTheme: String
  
  init(
    productName: String = "",
    productLogo: String = "",
    description: String = "",
    rating: Double = 0.0,
    latestVersion: String = "",
    publisher: String = "",
    colorTheme: String = "")
  {
    self.productName = productName
    self.productLogo = productLogo
    self.description = description
    self.rating = rating
    self.latestVersion = latestVersion
    self.publisher = publisher
    self.colorTheme = colorTheme
  }
}
