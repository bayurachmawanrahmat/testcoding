//
//  AllResponse.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation


import Foundation

struct AllResponse: Codable {
    let ok: Bool
    let message: String
    let status: Int
    let data: [Datas]
}

struct Datas: Codable {
    let productName, productLogo, description, latestVersion, publisher, colorTheme: String
    let rating: Double

  enum CodingKeys: String, CodingKey {
          case productName, productLogo, publisher, colorTheme, description, latestVersion, rating
  }
}
