//
//  DataCell.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import UIKit

protocol AllDataCell {
  var productName: String {get}
  var productLogo: String {get}
  var description: String {get}
  var rating: Double {get}
  var latestVersion: String {get}
  var publisher: String {get}
  var colorTheme: String {get}

}

class AllDataCell: UITableViewCell {
    @IBOutlet weak var vwCard: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    
    
    
//  @IBOutlet weak var vwCard: UIView!
//  @IBOutlet weak var imgThumbnail: UIImageView!
//  @IBOutlet weak var lblRestoName: UILabel!
//  @IBOutlet weak var lblRestoCity: UILabel!
//  @IBOutlet weak var lblRestoDesc: UILabel!
  
  
  override func draw(_ rect: CGRect) {
super.draw(rect)
    
    let shadowPath = UIBezierPath(roundedRect: vwCard.bounds, cornerRadius: 4)
    vwCard.layer.cornerRadius = 4
    vwCard.layer.masksToBounds = true
    vwCard.layer.shadowColor = UIColor.lightGray.cgColor
    vwCard.layer.shadowOpacity = 0.7
    vwCard.layer.shadowPath = shadowPath.cgPath
  }
  
  var interface: DataCell? {
    didSet{
      if let interface = interface {
//        lblRestoName.text = interface.restoName
//        lblRestoCity.text = interface.restoCity
//        lblRestoDesc.text = interface.restoDescription
//        if let urlImage = URL(string: "https://restaurant-api.dicoding.dev/images/small/" + interface.restoThumbnail){
//          imgThumbnail.sd_setImage(
//            with: urlImage,
//          placeholderImage: UIImage(),
//          options: [.allowInvalidSSLCertificates, .retryFailed],
//          context: nil)
//        }
      }
    }
  }
  
  
}
