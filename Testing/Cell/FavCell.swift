//
//  FavCell.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation
import UIKit

protocol DataFav {
    var productid: String {get}
    var productName: String {get}
}


class FavCell: UITableViewCell {
    
    @IBOutlet weak var btnHapus: UIButton!
    @IBOutlet weak var lblNama: UILabel!
    
    var db:DB_Manager = DB_Manager()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }

    var interface: DataFav? {
      didSet{
        if let interface = interface {
            lblNama.text = interface.productName
        }
      }
    }
    
    @IBAction func deleteTap(_ sender: Any){
        print("HAPUS")
//        
    }
}
