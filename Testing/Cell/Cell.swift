//
//  Cell.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import UIKit
import SDWebImage

protocol DataCell {
  var productName: String {get}
  var productLogo: String {get}
  var description: String {get}
  var rating: Double {get}
  var latestVersion: String {get}
  var publisher: String {get}
  var colorTheme: String {get}
}

class Cell: UITableViewCell {
    
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var vwCard: UIView!
    @IBOutlet weak var lblPublisher: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    override func draw(_ rect: CGRect) {
  super.draw(rect)
      
      let shadowPath = UIBezierPath(roundedRect: vwCard.bounds, cornerRadius: 4)
      vwCard.layer.cornerRadius = 4
      vwCard.layer.masksToBounds = true
      vwCard.layer.shadowColor = UIColor.lightGray.cgColor
      vwCard.layer.shadowOpacity = 0.7
      vwCard.layer.shadowPath = shadowPath.cgPath
    }
    
    var interface: DataCell? {
      didSet{
        if let interface = interface {
            lblNama.text = interface.productName
            lblDescription.text = interface.description
            lblRating.text = "Rating: " + String(interface.rating)
            lblVersion.text = "Versi: " + interface.latestVersion
            lblPublisher.text = "Publisher: " + interface.publisher
            if let urlImage = URL(string: interface.productLogo){
                imgProduct.sd_setImage(
                with: urlImage,
              placeholderImage: UIImage(),
              options: [.allowInvalidSSLCertificates, .retryFailed],
              context: nil)
            }
//            let warna = Color(hex: interface.colorTheme)
//            vwCard.backgroundColor = warna
            
        }
      }
    }
}
