//
//  FavoriteViewController.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//


import Foundation
import UIKit

class FavoriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var favTable: UITableView!
        
    let cellReuseIdentifier = "FavCell"
        
    var db:DB_Manager = DB_Manager()
    
    private var favoriteModels: [FavoriteModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName:"FavCell", bundle: nil)
        favTable.register(nib, forCellReuseIdentifier: "FavCell")
        favTable.delegate = self
        favTable.dataSource = self
        
        favoriteModels = db.read()
    }
    
    @IBAction func doneTap(_ sender: Any){
        dismiss(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return favoriteModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"FavCell", for: indexPath) as! FavCell
        cell.lblNama.text = favoriteModels[indexPath.row].name
        cell.btnHapus.tag = favoriteModels[indexPath.row].id
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        let kode = favoriteModels[indexPath.row].id
        print(kode)
        if editingStyle == .delete {
            favoriteModels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            db.deleteByID(id:kode )
        }
    }
}





