//
//  DetailViewController.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var imgData: UIImageView!
    @IBOutlet weak var lblDataDesc: UILabel!
    
    @IBOutlet weak var lblPublisher: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblVersi: UILabel!
    @IBOutlet weak var lblnamaProduk: UILabel!
    @IBOutlet weak var doneBUtton: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    
    var dataTlkm: DataModels?
    var db:DB_Manager = DB_Manager()
    
    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
      super.viewDidLoad()
        if let datas =  self.dataTlkm {
            if let urlImage = URL(string: datas.productLogo){
          imgData.sd_setImage(
          with: urlImage,
          placeholderImage: UIImage(),
          options: [.allowInvalidSSLCertificates, .retryFailed],
          context: nil)
                lblnamaProduk.text = datas.productName
                lblDataDesc.text = datas.description
                lblPublisher.text = "Publisher: " + datas.publisher
                lblRating.text = "Rating: " + String(datas.rating)
                lblVersi.text = "Versi: " + datas.latestVersion
          self.navigationItem.title = datas.productName
       }
     }
     
}
    
    public func setProperty(dataweh: DataModels){
      self.dataTlkm = dataweh
   }
    
    @IBAction func doneTap(_ sender: Any){
        dismiss(animated: true)
    }
    
    @IBAction func favoriteTap(_ sender: Any){
        if let datas =  self.dataTlkm
        {
            let nmProduk = datas.productName
            let descProduk = datas.description
            let pubProduk = datas.publisher
            let rtProduk = String(datas.rating)
            let vsProduk = datas.latestVersion
            let lgProduk = datas.productLogo
            let clProduk = datas.colorTheme
            
            db.insert(id: 0, name:nmProduk, desc:descProduk,pub:pubProduk,vers:vsProduk,rat:rtProduk,logo:lgProduk,bgcolor:clProduk)
        }
    }
}
    
