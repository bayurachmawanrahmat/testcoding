//
//  ViewController.swift
//  Testing
//
//  Created by Bayu Rachmawan Rahmat on 08/11/22.
//

import UIKit
import JGProgressHUD

protocol DataCellInterface: DataCell {}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblData: UITableView!
    
    private let cellId = "Cell"
    private let allDataViewModel = AllDataViewModel()
    
    private lazy var loadingDialog: JGProgressHUD = {
      let hud = JGProgressHUD()
      hud.textLabel.text = "Please Wait"
      return hud
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        allDataViewModel.reqGetAllData(isLoad: { state in
          self.showProgressDialog(state: state)
        }) {
          self.tblData.reloadData()
        }
    }
    
    private func setupLayout() {
        registerCell()
        self.navigationItem.title="List"
    }
    
    private func registerCell(){
            tblData.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
            tblData.delegate = self
            tblData.dataSource = self
            tblData.tableFooterView = UIView()
            tblData.separatorStyle = .none
    }
    
    private func showProgressDialog(state: Bool){
      if state {
        loadingDialog.show(in: self.view)
      }else {
        loadingDialog.dismiss(animated: true)
      }
    }
    
    @IBAction func showfavoriteTap(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Favorite") as! FavoriteViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let datas = allDataViewModel.getAllData(from: indexPath)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! DetailViewController
        vc.setProperty(dataweh: datas)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return allDataViewModel.numRowsAllData
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
      as? Cell
      else { return UITableViewCell() }
      
      cell.interface = allDataViewModel.getAllData(from: indexPath)
      return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
      return 131
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableView.automaticDimension
    }
}


